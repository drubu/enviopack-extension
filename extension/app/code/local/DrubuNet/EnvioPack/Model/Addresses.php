<?php
/**
 * Created by PhpStorm.
 * User: argdev
 * Date: 11/03/17
 * Time: 21:14
 */
class DrubuNet_EnvioPack_Model_Addresses extends DrubuNet_EnvioPack_Model_Core
{
    /**
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = array();
        $addresses = $this->getAddresses();

        foreach ($addresses as $address) {
            $label = sprintf(
                "%s %s %s %s (%s, %s)",
                $address['calle'],
                $address['numero'],
                $address['piso'],
                $address['depto'],
                $address['localidad'],
                $address['codigo_postal']
            );
            $result[] = array('value'=> $address['id'], 'label'=> $label);
        }

        return $result;
    }

    /**
     * Get addresses from EnvioPack settings
     * @return array
     */
    public function getAddresses()
    {
        $url = sprintf(
            "%s%s",
            $this->_apiUrl,
            'direcciones-de-envio'
        );

        $params = array(
            'access_token' => $this->getToken()
        );

        return $this->getData($url, $params);
    }
}