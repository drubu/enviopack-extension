<?php

/**
 * Created by PhpStorm.
 * User: Pablo Garcia
 * Company: DrubuNet
 * Date: 16/02/17
 * Time: 11:06
 */
abstract class DrubuNet_EnvioPack_Model_Core
{
    const SHIPPING_INFO_TYPE = 2;
    const SHIPPING_INFO_SERVICE = 3;
    const SHIPPING_INFO_SHIPPING_NAME = 4;

    const ENVIO_DOMICILIO = 'D';
    const ENVIO_SUCURSAL = 'S';

    protected $_apiUrl, $_apiKey, $_secretKey;

    public function __construct()
    {
        $this->_apiUrl = Mage::helper('drubunet_enviopack')->getConfig('api_url'); //'https://api.enviopack.com/';
        $this->_apiKey = Mage::helper('drubunet_enviopack')->getConfig('api_key'); //'cf1d515e19efdeee14659fed9a8bd4656e258813';
        $this->_secretKey = Mage::helper('drubunet_enviopack')->getConfig('secret_key'); //'02db10b65ee48af0e65b9ccec000d4e2d60f2100';
    }

    public function getToken()
    {
        $url = $this->_apiUrl . 'auth';

        $content = array(
            'api-key' => $this->_apiKey,
            'secret-key' => $this->_secretKey,
        );

        $response = $this->sendData($url, $content);

        return $response['token'];
    }

    protected function sendData($url, $content)
    {
        if (strpos($url, 'http') === false) {
            $url = rtrim($this->_apiUrl, '/') . '/' . ltrim($url, '/');
        }

        $content = json_encode($content);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        $json_response = curl_exec($curl);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($status != 200) {
            Mage::log("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
        }

        curl_close($curl);

        $response = json_decode($json_response, true);

        return $response;
    }

    protected function getData($url, $params, $decodeJson = true)
    {
        if (strpos($url, 'http') === false) {
            $url = rtrim($this->_apiUrl, '/') . '/' . ltrim($url, '/');
        }

        $queryString = '';
        foreach ($params as $key => $value) {
            if ($value) {
                $queryString .= "$key=$value&";
            }
        }

        if ($queryString) $url .= '?' . rtrim($queryString, "&");

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $json_response = curl_exec($curl);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if ($status != 200) {
            //Mage::log("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
            $response = false;
        } else {
            $response = $decodeJson ? json_decode($json_response, true) : $json_response;
        }

        return $response;
    }
}