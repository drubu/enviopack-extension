<?php

/**
 * Created by PhpStorm.
 * User: Pablo Garcia
 * Company: DrubuNet
 * Date: 16/02/17
 * Time: 11:06
 */
class DrubuNet_EnvioPack_Model_Rates extends DrubuNet_EnvioPack_Model_Core
{
    /** @var DrubuNet_EnvioPack_Helper_Data $_helper */
    private $_helper;

    /**
     * Instantiate the model with the proper credentials
     *
     * DrubuNet_EnvioPack_Model_Rates constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_helper = Mage::helper('drubunet_enviopack');
    }

    public function getCostRates($data)
    {
        $url = $this->_apiUrl . 'cotizar/costo';

        $content = array(
            'access_token' => $this->getToken(),
            'codigo_postal' => $data['postal_code'],
            'peso' => $data['weight'],
            'modalidad' => $data['mode'],
            'direccion_envio' => Mage::helper('drubunet_enviopack')->getConfig('address_from_id'),
            'orden_columna' => $data['order_by'],
            'orden_sentido' => $data['sort']
        );
        return $this->getData($url, $content);
    }

    public function getRates($data)
    {
        $accessToken = $this->getToken();

        $result = $this->getRatesByProvinceWithBranches($data, $accessToken);

        $url = $this->_apiUrl . 'cotizar/precio/por-correo';

        $content = array(
            'access_token' => $this->getToken(),
            'codigo_postal' => $data['codigoPostal'],
            'peso' => $data['peso'],
            'direccion_envio' => Mage::helper('drubunet_enviopack')->getConfig('address_from_id'),
            'modalidad' => 'D',
        );

        $result = array_merge($result, $this->getData($url, $content));

        return $result;
    }

    public function getRatesByCarrier($data)
    {
        $url = $this->_apiUrl . 'cotizar/precio/por-correo';

        $content = array(
            'access_token' => $this->getToken(),
            'codigoPostal' => $data['codigoPostal'],
            'peso' => $data['peso'],
            'volumen' => $data['volumen'],
            'correo' => $data['correo']
        );

        return $this->getData($url, $content);
    }

    public function getRatesWithBranches($data, $accessToken)
    {
        $result = array();

        //$accessToken = $this->getToken();
        $url = $this->_apiUrl . 'correos';

        $content = array(
            'access_token' => $accessToken,
            'filtrar_activos' => 1,
            'tiene_sucursales' => 1
        );

        $correos = $this->getData($url, $content);

        $url = $this->_apiUrl . 'cotizar/precio/por-correo';

        foreach ($correos as $correo) {


            $content = array(
                'access_token' => $accessToken,
                'codigo_postal' => $data['codigoPostal'],
                'peso' => $data['peso'],
                'direccion_envio' => Mage::helper('drubunet_enviopack')->getConfig('address_from_id'),
                'modalidad' => 'S',
                'correo' => $correo['id']
            );
            $result2 = $this->getData($url, $content);
            $result = array_merge($result, $result2);
        }

        return $result;
    }
    public function getRatesByProvince($data)
    {
        /** @var DrubuNet_EnvioPack_Helper_Data $helper */
        $helper = Mage::helper('drubunet_enviopack');

        /** @var DrubuNet_EnvioPack_Helper_Measurement $measurement */
        $measurement = Mage::helper('drubunet_enviopack/measurement');

        $accessToken = $this->getToken();

        $weightUnit = $helper->getWeightUnit();
        $quoteBy = $helper->getConfig('quote_by');
        if($quoteBy == DrubuNet_EnvioPack_Model_Quote::DIMENSIONS) {
            $weightUnit = $measurement->getUnitWeight();
            $weightUnit = $weightUnit['unit'];
        }
        if (
            ($quoteBy == DrubuNet_EnvioPack_Model_Quote::PACKAGE && $weightUnit == $measurement::ENVIOPACK_GR_CODE) ||
            ($quoteBy == DrubuNet_EnvioPack_Model_Quote::DIMENSIONS && $weightUnit != $measurement::ENVIOPACK_WEIGHT_UNIT)
        ) {
            //$peso = $measurement->getValueWeightConversion($weightUnit);
            $weight = $measurement->getValueWeightConversion($data['peso']);
        } else {
            $weight = $data['peso'];
        }

        $params = array(
            'provincia' => $data['provincia'],
            'direccion_envio' => $this->_helper->getAddressFromId(),
            'peso' => $weight//$data['peso']
        );
        $result = $this->getRatesByProvinceWithBranches($params, $accessToken);

        $url = $this->_apiUrl . 'cotizar/precio/por-provincia';

        $content = array(
            'access_token' => $accessToken,
            'provincia' => $data['provincia'],
            'peso' => $weight,//$data['peso'],
            'codigo_postal' => $data['codigoPostal'],
        );

        $result2 = $this->getData($url, $content);

        return array_merge($result, $result2);
    }

    public function getRatesByProvinceWithBranches($params, $accessToken = '')
    {
        $url = 'cotizar/precio/a-sucursal';

        if (!$accessToken) {
            $accessToken = $this->getToken();
        }

        $params['access_token'] = $accessToken;

        $result = $this->getData($url, $params);
        $newResult = array();
        if ($result) {
            $localidadList = array();
            foreach ($result as $item) {
                $localidadList[$item['sucursal']['correo']['id'] ."_".$item['valor']]['localities'][$item['sucursal']['localidad']['id']."_".$item['sucursal']['localidad']['nombre']][] = $item['sucursal'];
                $localidadList[$item['sucursal']['correo']['id'] ."_".$item['valor']]['valor'] = $item['valor'];
                $localidadList[$item['sucursal']['correo']['id'] ."_".$item['valor']]['modalidad'] = $item['modalidad'];
                $localidadList[$item['sucursal']['correo']['id'] ."_".$item['valor']]['servicio'] = $item['servicio'];
                $localidadList[$item['sucursal']['correo']['id'] ."_".$item['valor']]['horas_entrega'] = $item['horas_entrega'];
                $localidadList[$item['sucursal']['correo']['id'] ."_".$item['valor']]['correo'] = $item['sucursal']['correo'];
            }

            foreach ($localidadList as $row) {
                $method = sprintf(
                    "%s_%s_%s",
                    strtolower($row['modalidad']),
                    strtolower($row['servicio']),
                    $row['correo']['id']
                );
                $newResult["drubunet_enviopack_".$method."_".((int)$row['valor'])] = $row;
            }
        }

        Mage::unregister('branches');
        Mage::register('branches', $newResult);

        return $newResult;
    }
}
