<?php
/**
 * Created by PhpStorm.
 * User: argdev
 * Date: 11/03/17
 * Time: 21:14
 */
class DrubuNet_EnvioPack_Model_Statuses
{
    /**
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = array();
        $collection = Mage::getModel('sales/order_status')->getResourceCollection()->getData();
        foreach ($collection as $status) {
            $result[] = array('value'=>$status['status'], 'label'=>$status['label']);
        }
        return $result;

    }
}