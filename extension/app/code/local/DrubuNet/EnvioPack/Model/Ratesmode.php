<?php
/**
 * Created by PhpStorm.
 * User: argdev
 * Date: 11/03/17
 * Time: 21:14
 */
class DrubuNet_EnvioPack_Model_Ratesmode
{
    const RATES_MODE_SERVICE = 'service';
    const RATES_MODE_PROVINCE = 'province';
    /**
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = array(
            array('value'=> self::RATES_MODE_SERVICE, 'label'=> 'Correo'),
            array('value'=> self::RATES_MODE_PROVINCE, 'label'=>'Provincia'),
        );

        return $result;

    }
}