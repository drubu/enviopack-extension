<?php

class DrubuNet_EnvioPack_Adminhtml_OrdersController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        // Let's call our initAction method which will set some basic params for each action
        $this->_initAction()
            ->renderLayout();
    }

    public function newAction()
    {
        // We just forward the new action to a blank edit form
        $this->_forward('edit');
    }

    public function sortcheaperAction()
    {
        Mage::register('order_by', 'valor');
        // Let's call our initAction method which will set some basic params for each action
        $this->_initAction()
            ->renderLayout();
    }

    public function sortfasterAction()
    {
        Mage::register('order_by', 'horas_entrega');
        // Let's call our initAction method which will set some basic params for each action
        $this->_initAction()
            ->renderLayout();
    }

//    public function editAction()
//    {
//        $this->_initAction();
//
//        // Get id if available
//        $id = $this->getRequest()->getParam('id');
//        $model = Mage::getModel('drubunet_enviopack/order');
//
//        if ($id) {
//            // Load record
//            $model->load($id);
//
//            // Check if record is loaded
//            if (!$model->getId()) {
//                Mage::getSingleton('adminhtml/session')->addError($this->__('This order no longer exists.'));
//                $this->_redirect('*/*/');
//
//                return;
//            }
//        }
//
//        $this->_title($model->getId() ? $model->getName() : $this->__('New Order'));
//
//        $data = Mage::getSingleton('adminhtml/session')->getOrderData(true);
//        if (!empty($data)) {
//            $model->setData($data);
//        }
//
//        Mage::register('enviopack_order', $model);
//
//        $this->_initAction()
//            ->_addBreadcrumb($id ? $this->__('Edit Order') : $this->__('New Order'), $id ? $this->__('Edit Order') : $this->__('New Order'))
//            ->_addContent($this->getLayout()->createBlock('drubunet_enviopack/adminhtml_orders_edit')->setData('action', $this->getUrl('*/*/save')))
//            ->_initLayoutMessages('core/session')
//            ->_initLayoutMessages('adminhtml/session')
//            ->renderLayout();
//    }

//    public function saveAction()
//    {
//        if ($postData = $this->getRequest()->getPost()) {
//            $model = Mage::getModel('drubunet_enviopack/order');
//
//            if(!array_key_exists('active', $postData)) {
//                $postData['active'] = 0;
//            }
//            $model->setData($postData);
//
//            try {
//                //Validate if email exists
//                $collection = Mage::getResourceModel('drubunet_enviopack/order_collection')
//                    ->addFieldToFilter('email', array('eq' => $model->getEmail()));
//
//                if (is_null($model->getId()) && count($collection)) {
//                    Mage::getSingleton('adminhtml/session')->addError('The email already exists in the database.');
//                } else {
//
//                    $model->save();
//
//                    Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The order has been saved.'));
//                    $this->_redirect('*/*/');
//
//                    return;
//                }
//            } catch (Mage_Core_Exception $e) {
//                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
//            } catch (Exception $e) {
//                Mage::getSingleton('core/session')->addError($this->__('An error occurred while saving this order.'));
//            }
//
//            Mage::register('enviopack_order', $model);
//
//            $this->_initAction()
//                ->_addContent($this->getLayout()->createBlock('drubunet_enviopack/adminhtml_orders_edit')->setData('action', $this->getUrl('*/*/save')))
//                ->renderLayout();
//        }
//    }
//
//    public function deleteAction()
//    {
//        if ($this->getRequest()->getParam('id') > 0) {
//            try {
//                $model = Mage::getModel('drubunet_enviopack/order');
//                $model->setId($this->getRequest()
//                    ->getParam('id'))
//                    ->delete();
//                Mage::getSingleton('adminhtml/session')
//                    ->addSuccess('successfully deleted');
//                $this->_redirect('*/*/');
//            } catch (Exception $e) {
//                Mage::getSingleton('adminhtml/session')
//                    ->addError($e->getMessage());
//                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
//            }
//        }
//        $this->_redirect('*/*/');
//    }

    public function messageAction()
    {
        $data = Mage::getModel('drubunet_enviopack/order')->load($this->getRequest()->getParam('id'));
        echo $data->getContent();
    }

    public function downloadShipmentPdfAction()
    {
        $shipmentId = $this->getRequest()->getParam('shipment_id', false);
        if ($shipmentId) {
            /**
             * @var $api DrubuNet_EnvioPack_Model_Shipment
             */
            $api = Mage::getModel('drubunet_enviopack/shipment');

            $pdfContent = $api->getShippingLabel($shipmentId);

            if ($pdfContent) {
                $fileName = 'enviopack_etiqueta_'.$shipmentId.'.pdf';
                $this->_prepareDownloadResponse($fileName, $pdfContent);

            } else {
                Mage::getSingleton('adminhtml/session')
                    ->addNotice('El pedido no tiene etiqueta asociada.');
                $this->_redirect('*/*/');
            }
        }

    }

    public function showerrorAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function trackingAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function createshipmentAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function saveshipmentAction()
    {
        $orderId = $this->getRequest()->getParam('order_id', false);
        $shippingMethod = $this->getRequest()->getParam('shipping_method', false);
        $branch = $this->getRequest()->getParam('enviopack_branch', false);

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->load($orderId);

        /** @var DrubuNet_EnvioPack_Model_Shipment $api */
        $api = Mage::getModel('drubunet_enviopack/shipment');

        $order->setShippingMethod($shippingMethod);

        $shippingInfo = explode('_', $shippingMethod);

        $shipmentType = strtoupper($shippingInfo[2]);

        if ($shipmentType == DrubuNet_EnvioPack_Model_Shipment::ENVIO_SUCURSAL) {
            $order->setEnviopackBranch($branch);
        }

        $envioPackOrder = $api->createOrder($order);

        $envioPackShipment = $api->createShipment($order, $envioPackOrder['id']);

        $envioPackShipmentId = $envioPackShipment['id'];

        if ($envioPackShipmentId) {
            $order->setEnviopackShipmentId($envioPackShipmentId);

            $order->save();

            Mage::getSingleton('adminhtml/session')->addSuccess(
                "Envio generado exitosamente."
            );
        } else {
            Mage::getSingleton('adminhtml/session')->addNotice(
                "El envio no se pudo generar."
            );
        }

        $url = Mage::helper("adminhtml")->getUrl("*/*/index");
        echo '<script>window.opener.document.location.href = "'.$url.'";</script>';
        echo '<script>window.close();</script>';
    }

    public function massShipmentAction()
    {
        $orderIds = $this->getRequest()->getParam('entity_id', false);
        if ($orderIds) {
            $shippingMethods = array();
            if($this->getRequest()->getParam('shipping_methods', false)){
                $shippingMethods = $this->getRequest()->getParam('shipping_methods', false);
                $shippingMethodsExploded = explode(',', $shippingMethods);
                $shippingMethods = array();
                foreach ($shippingMethodsExploded as $item) {
                    list($key, $value) = explode('|', $item);
                    $shippingMethods = $shippingMethods + array($key => $value);
                }

            }

            /**
             * @var DrubuNet_EnvioPack_Model_Shipment $api
             */
            $api = Mage::getModel('drubunet_enviopack/shipment');

            //$shipmentIds = array();
            foreach ($orderIds as $orderId) {
                /** @var Mage_Sales_Model_Order $_order */
                $_order = Mage::getModel('sales/order')->load($orderId);
                if (is_null($_order->getEnviopackShipmentId())) {
                    $currentShipmentMethod = $_order->getShippingMethod();
                    if (array_key_exists($_order->getId(), $shippingMethods)) {
                        $_order->setShippingMethod('drubunet_enviopack_'.$shippingMethods[$_order->getId()]);
                    }

                    $envioPackOrderId = $_order->getEnviopackOrderId();

                    if(!$envioPackOrderId) {
                        $envioPackOrder = $api->createOrder($_order);
                        $envioPackOrderId = $envioPackOrder['id'];
                        if (array_key_exists('errors', $envioPackOrder)) {
                            $_order->setEnviopackTrackingNumber(json_encode($envioPackOrder, true));
                            $_order->setShippingMethod($currentShipmentMethod);
                            $_order->save();
                        }
                    }

                    if ($envioPackOrderId) {
                        $_order->setEnviopackOrderId($envioPackOrderId);

                        $envioPackShipment = $api->createShipment($_order, $envioPackOrderId);

                        if (array_key_exists('errors', $envioPackShipment)) {
                            $_order->setEnviopackTrackingNumber(json_encode($envioPackShipment, true));
                            $_order->setShippingMethod($currentShipmentMethod);
                        } else {
                            $_order->setEnviopackTrackingNumber('');
                        }

                        $envioPackShipmentId = $envioPackShipment['id'];
                        if ($envioPackShipmentId) {
                            $_order->setEnviopackShipmentId($envioPackShipmentId);
                            $_order->save();
                            $this->createMagentoShipment($_order);
                        } else {
                            $_order->save();
                        }
                    }
                }
            }

            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('tax')->__(
                    '%d registro(s) procesado(s).', count($orderIds)
                )
            );
        }

        $this->_redirect('*/*/index');
    }

    /**
     * Create Magento Shipment
     * @param Mage_Sales_Model_Order $order
     */
    private function createMagentoShipment($order)
    {
        /** @var DrubuNet_EnvioPack_Helper_Data $helper */
        $helper = Mage::helper('drubunet_enviopack');

        $helper->createMagentoShipment($order);
    }

    public function massPrintAction()
    {
        $orderIds = $this->getRequest()->getParam('entity_id', false);
        if ($orderIds) {
            /**
             * @var DrubuNet_EnvioPack_Model_Shipment $api
             */
            $api = Mage::getModel('drubunet_enviopack/shipment');

            $shipmentIds = array();
            foreach ($orderIds as $orderId) {
                $_order = Mage::getModel('sales/order')->load($orderId);
                if ($_order->getEnviopackShipmentId()) {
                    $shipmentIds[] = $_order->getEnviopackShipmentId();
                }
            }

            $shipmentIds = implode(',', $shipmentIds);

            $pdfContent = $api->getShippingLabel($shipmentIds);

            if ($pdfContent) {
                $fileName = 'enviopack_etiquetas.pdf';
                $this->_prepareDownloadResponse($fileName, $pdfContent);

            } else {
                Mage::getSingleton('adminhtml/session')
                    ->addNotice('No se encontraron etiquetas para los pedidos seleccionados.');
                $this->_redirect('*/*/');
            }
        }

    }

    /**
     * Initialize action
     *
     * Here, we set the breadcrumbs and the active menu
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {

        $this->loadLayout()
            // Make the active menu match the menu config nodes (without 'children' inbetween)
            ->_setActiveMenu('enviopacktab/drubunet_enviopack_orders')
            ->_title($this->__('EnvioPack'))->_title($this->__('Orders'))
            ->_addBreadcrumb($this->__('EnvioPack'), $this->__('EnvioPackTab'))
            ->_addBreadcrumb($this->__('Orders'), $this->__('Orders'));

        return $this;
    }

    /**
     * Check currently called action by permissions for current user
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('enviopacktab/ordenes');
    }
}