<?php
class DrubuNet_EnvioPack_NotificationController extends Mage_Core_Controller_Front_Action
{
    const NOTIFICATION_TYPE_PROCESSED = 'envio-procesado';

    public function indexAction()
    {
        $notificationType = $this->getRequest()->getParam('tipo', false);
        $shipmentId = $this->getRequest()->getParam('id', false);


        if ($notificationType && $notificationType == self::NOTIFICATION_TYPE_PROCESSED && $shipmentId) {
            $_order = Mage::getModel('sales/order')
                ->load($shipmentId, 'enviopack_shipment_id');

            if ($_order->hasData()) {
                /** @var DrubuNet_EnvioPack_Model_Shipment $api */
                $api = Mage::getModel('drubunet_enviopack/shipment');

                $shipmentInfo = $api->getShipmentInformation($shipmentId);
                $trackingNumber = $shipmentInfo['tracking_number'];

                $_order->setEnviopackTrackingNumber($trackingNumber);
                $_order->setEnviopackProcessed(true);
                $_order->save();

//                $trackingDetail = array(
//                    'carrier_code' => 'ups',
//                    'title' => 'United Parcel Service',
//                    'number' => 'TORD23254WERZXd3', // Replace with your tracking number
//                );
//
//                $track = Mage::getModel('sales/order_shipment_track')->addData($trackingDetail);
//                $shipment->addTrack($track);


//                $transactionSave = Mage::getModel('core/resource_transaction')
//                    ->addObject($shipment)
//                    ->addObject($shipment->getOrder())
//                    ->save();

                $resultMessage = sprintf(
                    "envio-procesado - ok - shipmentId: %s",
                    $shipmentId
                );
            } else {
                $resultMessage = sprintf(
                    "envio-procesado - error - shipmentId doesn't exist: %s",
                    $shipmentId
                );
            }
        } else {
            $resultMessage = sprintf(
                "envio-procesado - error - notificationType: %s - shipmentId: %s",
                $notificationType,
                $shipmentId
            );
        }
        Mage::log($resultMessage, null, 'enviopack.log');
    }
}