<?php
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$installer = new Mage_Sales_Model_Resource_Setup('core_setup');

$attribute  = array(
    'type' => 'int',
    'backend_type' => 'int',
    'frontend_input' => 'int',
    'is_user_defined' => true,
    'label' => 'enviopack_processed',
    'visible' => true,
    'required' => false,
    'user_defined' => false,
    'default' => null,
    'comparable' => false,
    'searchable' => false,
    'filterable' => false
);

$installer->addAttribute('order','enviopack_processed',$attribute);

$installer->endSetup();