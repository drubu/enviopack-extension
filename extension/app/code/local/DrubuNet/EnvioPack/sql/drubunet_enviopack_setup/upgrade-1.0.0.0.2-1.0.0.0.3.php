<?php
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer = $this;
$installer->startSetup();

$installer = new Mage_Sales_Model_Resource_Setup('core_setup');

$attribute  = array(
    'type' => 'text',
    'backend_type' => 'text',
    'frontend_input' => 'text',
    'is_user_defined' => true,
    'label' => 'enviopack_tracking_number',
    'visible' => true,
    'required' => false,
    'user_defined' => false,
    'default' => null,
    'comparable' => false,
    'searchable' => false,
    'filterable' => false
);

$installer->addAttribute('order','enviopack_tracking_number',$attribute);

$installer->endSetup();