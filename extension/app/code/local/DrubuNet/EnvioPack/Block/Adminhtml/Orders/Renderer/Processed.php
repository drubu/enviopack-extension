<?php
class DrubuNet_EnvioPack_Block_Adminhtml_Orders_Renderer_Processed extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row) {

        if($row->getEnviopackProcessed()) {
            $html = 'Si';
        } else {
            $html = 'No';
        }

        return $html;
    }
}