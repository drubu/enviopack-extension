<?php
class DrubuNet_EnvioPack_Block_Adminhtml_Orders_Renderer_Label extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row) {

        if ($row->getEnviopackShipmentId() && $row->getEnviopackProcessed()) {
            $url = Mage::helper("adminhtml")->getUrl('adminhtml/orders/downloadShipmentPdf').'shipment_id/'.$row->getEnviopackShipmentId();
            $html = '<a href="'.$url.'">Imprimir Etiqueta</a>';
        } else {
            $html = 'Etiqueta no disponible';
        }

        return $html;
    }
}