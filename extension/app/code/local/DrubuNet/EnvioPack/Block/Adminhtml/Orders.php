<?php

class DrubuNet_EnvioPack_Block_Adminhtml_Orders extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * DrubuNet_EnvioPack_Block_Adminhtml_Orders constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_blockGroup = 'drubunet_enviopack';
        $this->_controller = 'adminhtml_orders';
        $this->_headerText = $this->__('Orders');

        //Remove button always after parent constructor in order to have it working.
        $this->_removeButton('add');

        $this->_addButton('sort_cheaper', array(
            'label'   => Mage::helper('catalog')->__('Ordenar más económico'),
            'onclick' => "setLocation('{$this->getUrl('*/*/sortcheaper')}')",
            'class'   => 'add'
        ));
        $this->_addButton('sort_faster', array(
            'label'   => Mage::helper('catalog')->__('Ordener más rápido'),
            'onclick' => "setLocation('{$this->getUrl('*/*/sortfaster')}')",
            'class'   => 'add'
        ));
    }

    public function getError()
    {
        $orderId = $this->getRequest()->getParam('order_id', false);
        if($orderId) {
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel('sales/order')->load($orderId);
            $errorInfo = $order->getEnviopackTrackingNumber();
            $errorInfo = json_decode($errorInfo, true);
            if (is_array($errorInfo)) {
                return $errorInfo;
            }

        }
        return '';
    }

    public function getTrackingInformation()
    {
        $shipmentId = $this->getRequest()->getParam('shipment_id', false);
        $trackingInformation = array();
        if ($shipmentId) {
            /**
             * @var $api DrubuNet_EnvioPack_Model_Shipment
             */
            $api = Mage::getModel('drubunet_enviopack/shipment');

            $trackingInformation = $api->getTrackingInformation($shipmentId);

            //var_dump($trackingInformation);
        }
        return $trackingInformation;
    }

    public function getCreateShipmentUrl()
    {
        return Mage::helper("adminhtml")->getUrl('adminhtml/orders/saveshipment');
    }

    public function getOrderId()
    {
        if (!Mage::registry('enviopack_order_id')) {
            $orderId = $this->getRequest()->getParam('order_id', false);
            Mage::register('enviopack_order_id', $orderId);
        } else {
            $orderId = Mage::registry('enviopack_order_id');
        }

        return $orderId;
    }
    public function getRates()
    {
        $orderId = $this->getOrderId();

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->load($orderId);

        /** @var DrubuNet_EnvioPack_Model_Rates $api  */
        $api = Mage::getModel('drubunet_enviopack/rates');

        /** @var Mage_Sales_Model_Order_Address $shippingAddress  */
        $shippingAddress = $order->getShippingAddress();

        /** @var DrubuNet_EnvioPack_Helper_Data $helper */
        $helper = Mage::helper('drubunet_enviopack');


        $content = array(
            'codigoPostal' => $shippingAddress->getPostcode(),
            'peso' => $order->getWeight(),
            //'volumen' => $volumen
            'provincia' => 'C'
        );

        $rates = $api->getRates($content);

        $formattedRates = array();
        foreach ($rates as $data) {
            $mode = $helper->mapMode($data['modalidad']);
            $service = $helper->mapService($data['servicio']);
            $deliveryTime = $data['horas_entrega'];

            if(isset($data['correo'])) {
                $carrierName = $data['correo']['nombre'];
                $carrierId = $data['correo']['id'];

                $title = sprintf("%s - %s - %s (%s hs.)", $carrierName, $mode, $service, $deliveryTime);
            } else {
                $carrierId = 'custom';

                $title = sprintf("%s - %s (%s hs.)", $mode, $service, $deliveryTime);
            }

            $method = sprintf(
                "drubunet_enviopack_%s_%s_%s",
                strtolower($data['modalidad']),
                strtolower($data['servicio']),
                $carrierId
            );

            $formattedRates[] = array(
                'code' => $method,
                'title' => $title
            );
        }

        return $formattedRates;
    }
}