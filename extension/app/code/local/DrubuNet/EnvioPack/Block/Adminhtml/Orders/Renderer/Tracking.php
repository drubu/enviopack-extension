<?php
class DrubuNet_EnvioPack_Block_Adminhtml_Orders_Renderer_Tracking extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row) {

        $trackingNumber = $row->getEnviopackTrackingNumber();
        if($row->getEnviopackShipmentId() && $row->getEnviopackProcessed()) {
            $url = Mage::helper("adminhtml")->getUrl('adminhtml/orders/tracking').'shipment_id/'.$row->getEnviopackShipmentId();
            $html = '<a href="#" id="linkId" onclick="popWin(\''.$url.'\',\'trackshipment\',\'width=800,height=600,resizable=yes,scrollbars=yes\')" title="'.$trackingNumber.'">'.$trackingNumber.'</a>';
        } else if ($trackingNumber) {
            $errors = json_decode($trackingNumber, true);
            if (is_array($errors)) {
                $label = 'Ver Error';
                $url = Mage::helper("adminhtml")->getUrl('adminhtml/orders/showerror').'order_id/'.$row->getId();
                $html = '<a href="#" id="linkId" onclick="popWin(\''.$url.'\',\'trackshipment\',\'width=800,height=600,resizable=yes,scrollbars=yes\')" title="'.$label.'">'.$label.'</a>';
            } else {
                $html = 'Tracking no disponible';
            }
        } else {
            $html = 'Tracking no disponible';
        }

        return $html;
    }
}