<?php
class DrubuNet_EnvioPack_Block_Adminhtml_Orders_Renderer_Mode extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row) {

		if($row->getEnviopackBranch())
		{
			$html = 'Sucursal';
		}
		else
		{
			$html = 'Domicilio';
		}

        return $html;
    }
}