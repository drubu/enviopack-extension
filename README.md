# Magento - módulo EnvioPack #
# Verson 1.0.2 #

# Extensión para la integración con **EnvioPack** #

* [Instalación](https://bitbucket.org/drubu/enviopack-extension/overview#markdown-header-instalacion)
* [Configuración](https://bitbucket.org/drubu/enviopack-extension/overview#markdown-header-configuracion)
* [Tablas](https://bitbucket.org/drubu/enviopack-extension/overview#markdown-header-tablas)
* [Gestión](https://bitbucket.org/drubu/enviopack-extension/overview#markdown-header-gestion)
* [Templates de Ejemplo para Checkout](https://bitbucket.org/drubu/enviopack-extension/overview#markdown-header-templates-de-ejemplo-para-checkout)
* [Configuración de Dimensiones para la Cotización de Envios](https://bitbucket.org/drubu/enviopack-extension/overview#markdown-header-configuracion-de-dimensiones-para-la-cotizacion-de-envios)


## Instalación
1.	Copiar carpeta app al root de magento con los mismos nombres.
2.	Refrescar cache: System->Cache Managment.
3.	En System->Configuration, opción lateral sales->Shipping Methods y configurar <strong>EnvioPack</strong>.

Es necesaria la libreria soap ya que la extensión usa *SoapClient* para la conexión con el server de EnvioPack.


## Configuración

1. Enabled: Habilitar/Deshabilitar la extensión.
2. Title: Titulo que se mostrara en en el checkout.
3. Api Url: Es la url a la api de EnvioPack.
4. ApiKey: Es la key generada en el sitio de EnvioPack (Usuario -> Configuraciones de API).
5. Api Secret Key: Es la secret key generada en el sitio de EnvioPack (Usuario -> Configuraciones de API).
6. Modo: Correo/Provincia (En modo correo mostrara en el checkout los precios de cada operador logistico y en modo Provincia los precios configurados en EnvioPack).
7. Paquetes: Listado de paquetes configurados en EnvioPack.
8. Direcciones: Direcciones de origen cargadas en EnvioPack.
9. Generacion Automatica: Si/No.
10. Status: Estado en el que el envio se generara automaticamente si el campo Generacion Automatica se configuro como si.
11. Sort Order: Orden en el que aparecera en el checkout.

La extension modifica el phtml available.phtml para incluir las opciones de seleccionar localidad y sucursal para el caso del envio a sucusarl. Se adjunta un ejemplo en la carpeta template para que pueda ser aplicado en el phtml.

Tambien sera necesario que se habilite la opcion de cantidad de campos para calle a 4, ya que EnvioPack usara cada uno para calle, numero, piso y numero. 

En la cuenta de EnvioPack, en Usuario -> Configuraciones de API, deberan ingresar la url de notificaciones, donde EnvioPack informara cuando un envio se haya procesado, de esta forma se vera reflejado en Magento.
La ruta debe ser dominio.com/enviopack/notification

## Tablas

EnvioPack requiere las provincias normalizadas de la siguiente manera:

 Provincia           | Código 
---------------------|---------
 CABA                | C       
 Buenos Aires        | B       
 Catamarca           | K       
 Chaco               | H       
 Chubut              | U       
 Córdoba      		 | X       
 Corrientes          | W       
 Entre Ríos          | E       
 Formosa             | P       
 Jujuy               | Y       
 La Pampa            | L       
 La Rioja            | F       
 Mendoza             | M       
 Misiones            | N       
 Neuquén             | Q       
 Río Negro           | R       
 Salta               | A       
 San Juan            | J       
 San Luis            | D       
 Santa Cruz          | Z       
 Santa Fe            | S       
 Santiago del Estero | G       
 Tierra del Fuego    | V       
 Tucumán             | T       


## Gestión

EnvioPack permite gestionar de forma independiente las ordenes que utilicen como metodo de envio EnvioPack. En el menu principal EnvioPack -> Ordenes encontraran una grilla desde la cual podran realizar gestiones individuales como asi tambien gestiones masivas. Entre ellas se destacan:

1. Impresion Masiva.
2. Generacion Masiva de comprobantes.
3. Informacion de tracking.
4. Generacion de envios de forma manual o automatica.

## Templates de Ejemplo para Checkout
Se adjunta 1 template de ejemplo con el código necesario para mostrar las sucursales en caso de que la opción de envío sea a sucursal:

1. /app/design/frontend/[PACKAGE]/[THEME]/template/checkout/onepage/shipping_method/available.phtml.

Se adjuntan 2 templates que ejemplifican como implementar el campo "street address" subdividido en 4 campos, con las validaciones necesarias para EnvioPack.
Los mismos deben ir ubicados dentro de las siguientes carpetas:

1. /app/design/frontend/[PACKAGE]/[THEME]/template/persistent/checkout/onepage/billing.phtml- para la dirección de facturación (Billing address).
2. /app/design/frontend/[PACKAGE]/[THEME]/template/checkout/onepage/shipping.phtml- para la dirección de envio (Shipping address).

Estos templates son un ejemplo que detallan la siguiente implementacion:

1. Configurar en [system>configuration>customers>customer configuration>name and address options] en la opción [number of lines in a street address] que el numero de lineas sea 4.
2. Como se muestra en los templates, en cada subcampo del campo [street] se debe validar de la siguiente forma.

Nombre 		| Descripción                                                                 |Validación 
------------|-----------------------------------------------------------------------------|--------------------------------------------------------------------------------
street1     |Parametro calle, obligatorio de 30 caracteres alfanumericos con espacios     |(class="input-text required-entry validate-alphanum-with-spaces" maxlength="30") 
street2     |Parametro número, obligatorio de 5 caracteres numericos                      |(class="input-text required-entry validate-number" maxlength="5") 
street3     |Parametro piso, opcional de 6 caracteres alfanumericos con espacios          |(class="input-text validate-alphanum-with-spaces" maxlength="6") 
street4     |Parametro departamento, opcional de 4 caracteres alfanumericos con espacios  |(class="input-text validate-alphanum-with-spaces" maxlength="4") 

4. Como recomendación, tambien en el ejemplo, se recomiendo reordenar los campos [country_id] [region_id] y [city] en ese orden.
5. Se recuerda que tambien se deben modificar los templates de la parte de direcciones de la cuenta de cliente.

## Configuración de Dimensiones para la Cotización de Envios
Enviopack utiliza 2 tipos de cotización para las ordenes. Ambos tipos se configuraran en En System->Configuration, opción lateral sales->Shipping Methods y configurar <strong>EnvioPack</strong>.
Los tipos son los siguientes:

1. Por Paquete: Este metodo permitira elegir en la opcion [paquetes] un paquete previamente creado en https://app.enviopack.com/configuracion/mis-paquetes. Para cotizar, esta opcion utilizara las dimensiones del paquete seleccionado excepto el peso, que sera tomado de los productos.
2. Por Producto: Este metodo tomara las dimensiones y peso desde los productos. Para esto, en la opcion [Product attributes mapping] hay que elegir los atributos de producto que informen cada una de las dimensiones (largo, ancho, alto y peso del producto). Para esto, los atributos deberan ser creados previamente en Magento y completados en cada uno de los productos. Enviopack cotizara de forma mas precisa calculando el aforo de todos los productos de la orden.

